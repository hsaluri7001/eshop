import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UserService } from 'src/app/userservice/user.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
export interface User{
 userName:string,
 email:string,
 phoneNumber:number,
 passWord:string

}
/**
 *
 * @export
 * @class RegistrationComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  login: boolean = true;
  registration: boolean = false;
  registrationForm: FormGroup;
  submitted: boolean = false;
  registerUserSubscribe: Subscription;
  constructor(private userService: UserService,
   private snackBar:MatSnackBar) { }

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      userName: new FormControl('', [Validators.required, Validators.minLength(5)]),
      email: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[bbc|abc]+\.[a-z]{2,}$')]),
      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      passWord: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)])
    })
  }
  /**
   * @description Changes the flag to switch between login and registration
   * @memberof RegistrationComponent
   */
  loginFlag = (flag) => {
    this.login = flag;
    this.registration = !flag;
    console.log(this.login);
    console.log(this.registration)
  }
  /**
   * @description Register's the user to the server by doing validations ,
   * @memberof RegistrationComponent
   */
  registerUser = () => {
    this.submitted = true;
    console.log(this.registrationForm.value);
    let formValue = this.registrationForm.value;
    if (this.registrationForm.valid) {
       let user:User= {
        userName: formValue.userName,
        email: formValue.email,
        phoneNumber: formValue.phoneNumber,
        passWord: formValue.passWord
      }
      this.registerUserSubscribe = this.userService.registerUser(formValue).subscribe((response) => {
        if (response !== null) {
          console.log(response);
          this.snackBar.open("Registered, Please Login","Registerd",{duration:2000,
            horizontalPosition: "center",
            verticalPosition: "top"})
          this.login = true;
          this.registration = false;
        }
        else {
          alert("please try again");
          this.snackBar.open("Please try again!", "error",{
            duration: 2000})
        }
      }, (error) => {
        alert('NetWork error!please try after some time')
      })
    }
  }
  /**
   * @description Return's the msg based on error validation
   * @memberof RegistrationComponent
   */
  getUserNameErrorMessage = () => {
    return this.registrationForm.controls.userName.hasError('required') ? 'You must enter user name' :
      this.registrationForm.controls.userName.hasError('minlength') ? 'Required length is at least 5 characters' :
        '';
  }
  /**
   * @description Return's the msg based on error validation for mail
   * @memberof RegistrationComponent
   */
  getEmailErrorMessage = () => {
    return this.registrationForm.controls.email.hasError('required') ? 'You must enter email' :
      this.registrationForm.controls.email.hasError('pattern') ? 'Email must be bbc or abc domain' : '';
  }
  /**
   *
   * @memberof RegistrationComponent
   */
  getPhoneNumberErrorMessage = () => {
    return this.registrationForm.controls.phoneNumber.hasError('required') ? 'You must enter phoneNumber' :
      this.registrationForm.controls.email.hasError('minlength') ? 'Please enter valid phone number' :
        this.registrationForm.controls.email.hasError('maxlength') ? 'Please enter valid phone number' : '';
  }
  /**
   *
   * @memberof RegistrationComponent
   */
  getPassWordErrorMessage = () => {
    return this.registrationForm.controls.passWord.hasError('required') ? 'You must enter passWord' :
      this.registrationForm.controls.passWord.hasError('pattern') ? 'Password must contains atleast one special character,one lower case and one upper case & min 8 characters' :
        this.registrationForm.controls.passWord.hasError('minlength') ? 'Password must contains atleast one special character,one lower case and one upper case & min 8 characters' :
          '';
  }
  /**
   * @description unsubscribing registerUserSubcribe
   * @memberof RegistrationComponent
   */
  ngOnDestroy() {
    if (this.registerUserSubscribe && this.registerUserSubscribe.closed) {
      this.registerUserSubscribe.unsubscribe();
    }
  }
}
/* 
function emailValidator(control: AbstractControl): { [key: string]: boolean } | null {
  let email = control.value;
  return null
} */