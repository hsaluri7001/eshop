import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RegistrationComponent } from 'src/app/registration/registration/registration.component';
import { ServiceEmitterService } from 'src/app/serviceEmitter/service-emitter.service';
import { Router } from '@angular/router';
/**
 *
 *
 * @export
 * @class NavbarComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  count: number = 0;
  profile: any;
  loginButtonFlag: boolean = true;
  constructor(private dialog: MatDialog,
    private serviceEmitter: ServiceEmitterService,
    private router: Router,
    private cartCountEmitter:ServiceEmitterService) {
    if (sessionStorage.getItem("profile") !== null) {
      this.profile = JSON.parse(sessionStorage.getItem("profile"));
      this.loginButtonFlag = false;
    }
  }

  ngOnInit(): void {
    if (this.serviceEmitter.subVar == undefined) {
      this.serviceEmitter.subVar = this.serviceEmitter.profileFlagEmitter.subscribe(() => {
        this.loginButtonFlag = false;
        this.profile = JSON.parse(sessionStorage.getItem("profile"));
      });
    }
    if (this.serviceEmitter.carVar == undefined) {
      this.serviceEmitter.carVar = this.serviceEmitter.cartItemCount.subscribe((count) => {
        console.log(count);
        this.count = count;
      });
    }
  }
  /**
   *
   *
   * @memberof NavbarComponent
   */
  userCart = () => {
    if (sessionStorage.getItem("profile") !== null) {
      this.router.navigate(['cart']);
    }
    else {
      this.loginPage();
    }
  }
  /**
   * @description popup the login page
   * @memberof NavbarComponent
   */
  loginPage = () => {
    let dialogRef = this.dialog.open(RegistrationComponent, {
      height: '528px',
      width: '678px'
    });
  }
  /**
   *
   * @description clear's the user session when user click on logout
   * @memberof NavbarComponent
   */
  clearSession = () => {
    this.loginButtonFlag = true;
    this.profile = false;
    this.router.navigate(['']);
    sessionStorage.removeItem("profile");
    this.serviceEmitter.makeCartDefault();
    
    
  }
}
