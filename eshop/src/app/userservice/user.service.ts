import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../registration/registration/registration.component';
/**
 *
 * @export
 * @class UserService
 */
@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private httpRequest:HttpClient) { }
 /**
  * @Description Service to add the user 
  * @memberof UserService
  */
 registerUser=(user:User)=>{
    return this.httpRequest.post(`${environment.baseUrl}/users`,user)
  }
  /**
   * @description Service to validate the user whether user already exits or not
   * @memberof UserService
   */
  getUser=(email:string)=>{
    return this.httpRequest.get(`${environment.baseUrl}/users?email=${email}`);
  }
  /**
   * @description Service to validate the credentials for login
   * @memberof UserService
   */
  getUserAuthentication=(userName,passWord)=>{
    let url:string=`${environment.baseUrl}/users?userName=${userName}&&passWord=${passWord}`;
    return this.httpRequest.get(url);
  }

}