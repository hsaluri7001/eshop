import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
/**
 *
 *
 * @export
 * @class ServiceEmitterService
 */
@Injectable({
  providedIn: 'root'
})

export class ServiceEmitterService {

  profileFlagEmitter=new EventEmitter();
  cartItemCount=new EventEmitter();
  subVar:Subscription;
  carVar:Subscription;
  count=0;
  constructor() { }
 /**
  * @description Changes the flag to update login button and profile button
  * @memberof ServiceEmitterService
  */

 setProfile=()=>{
   this.profileFlagEmitter.emit(true);
 }
 /**
  * @description Set the count for cartbadge 
  * @memberof ServiceEmitterService
  */
 addCartCount=()=>{
   this.count=this.count+1;
   this.cartItemCount.emit(this.count);
 }
 removeCartCount=()=>{
   this.count=this.count-1;
   this.cartItemCount.emit(this.count);
 }
makeCartDefault(){
  this.count=0;
  this.cartItemCount.emit(this.count);
} 

}
