import { TestBed } from '@angular/core/testing';

import { ServiceEmitterService } from './service-emitter.service';

describe('ServiceEmitterService', () => {
  let service: ServiceEmitterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceEmitterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
