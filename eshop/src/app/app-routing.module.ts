import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { LoginComponent } from './login/login/login.component';
import { RegistrationComponent } from './registration/registration/registration.component';
import { NavbarComponent } from './navvar/navbar/navbar.component';
import { EcartComponent } from './cart/ecart/ecart.component';
import { PreviousOrdersComponent } from './previousOrders/previous-orders/previous-orders.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';


const routes: Routes = [  
  {path:'',component:DashboardComponent},
  {path:'cart',component:EcartComponent},
  {path:'login',component:LoginComponent},
  {path:'registration',component:RegistrationComponent},
  {path:'previousOrders',component:PreviousOrdersComponent},
  {path:'**',component:PageNotFoundComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
