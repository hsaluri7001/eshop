import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/orders/order.service';
import { MatDialog } from '@angular/material/dialog';
import { RegistrationComponent } from 'src/app/registration/registration/registration.component';
import { Subscription } from 'rxjs';
/**
 *
 *
 * @export
 * @class PreviousOrdersComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-previous-orders',
  templateUrl: './previous-orders.component.html',
  styleUrls: ['./previous-orders.component.css']
})

export class PreviousOrdersComponent implements OnInit {
  profile: any = null;
  listOfOrders: any;
  previousOrderSubscribe: Subscription;

  constructor(private orderService: OrderService, private dialog: MatDialog) {
    this.profile = JSON.parse(sessionStorage.getItem('profile'));
    this.getPreviousOrders();
  }
  ngOnInit(): void {
  }
  /**
   * @description Get the previous orders when user click's on the previous orders
   * @memberof PreviousOrdersComponent
   */
  getPreviousOrders = () => {
    if (sessionStorage.getItem("profile") !== null) {
      let email = this.profile.email;
      this.previousOrderSubscribe = this.orderService.getPreviousOrders(`${email}`).subscribe((response) => {
        console.log(response);
        this.listOfOrders = response;
      }, (error) => {
        console.log(error);
      });
    }
    else {
      alert("please login!");
      let dialogRef = this.dialog.open(RegistrationComponent, {
        height: '528px',
        width: '678px'
      });
    }
  }
  /**
   * @description unsubscribing previousOrderSubscribe
   * @memberof PreviousOrdersComponent
   */
  ngOnDestroy() {
    if (this.previousOrderSubscribe && this.previousOrderSubscribe.closed) {
      this.previousOrderSubscribe.unsubscribe();
    }

  }

}
