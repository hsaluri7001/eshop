import { Component, OnInit } from '@angular/core';
import { AllPhonesService } from 'src/app/phones/all-phones.service';
import { MatDialog } from '@angular/material/dialog';
import { RegistrationComponent } from 'src/app/registration/registration/registration.component';
import { CartService } from 'src/app/cartService/cart.service';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
export interface Item {
  header: string,
  itemName: string,
  price: number,
  quantity: number,
  totalBill: number
}
/**
 *
 *
 * @export
 * @class DashboardComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  allPhones: any;
  selectedItems: Array<any> = [];
  loginButtonFlag: boolean = true;
  getPhonesSubscription: Subscription;
  constructor(private phonesService: AllPhonesService,
    private dialog: MatDialog, private cartService: CartService, private snackBar: MatSnackBar) {
    this.getAllPhones();
  }
  ngOnInit(): void {
  }

  /**
   * @description Loads the phone's from server 
   * @memberof DashboardComponent
   */
  getAllPhones = () => {
    this.getPhonesSubscription = this.phonesService.getAllPhones().subscribe((response) => {
      this.allPhones = response;
    }),
      (error) => {
        console.log(error)
      }
  }
  /**
   * @description Items will be added to cart when user click on add to cart
   * @memberof DashboardComponent
   */
  addToCart = (product) => {
    if (sessionStorage.getItem("profile") !== null) {
      for (let some of this.selectedItems) {
        if (product.itemName === some.itemName) {
          let alert = "Item already added to cart";
          this.openSnackBar(alert, "cart");
          return false;
        }
      }
      const { itemName, price, header } = product
      let item: Item = {
        quantity: 1,
        itemName,
        price,
        header,
        totalBill: parseInt(price)
      }
      this.selectedItems.push(item);
      this.cartService.addToCart(item);
    }
    else {
      this.loginPage();
    }
  }
  /**
   * @description Below method pop up's the login page  
   * @memberof DashboardComponent
   */
  loginPage = () => {
    let dialogRef = this.dialog.open(RegistrationComponent, {
      height: '528px',
      width: '678px'
    });
  }
  /**
   * @description loads the profile
   * @memberof DashboardComponent
   */
  getProfile = () => {
    return sessionStorage.getItem("profile");
  }
  /**
   *@description unsubscribing getPhonesSubcription
   *
   * @memberof DashboardComponent
   */
  ngOnDestroy() {
    if (this.getPhonesSubscription && this.getPhonesSubscription.closed) {
      this.getPhonesSubscription.unsubscribe();
    }
  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}

