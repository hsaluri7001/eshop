import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
/**
 *Creates an instance of OrderService.
 * @param {HttpClient} httpRequest
 * @memberof OrderService
 */
constructor(private httpRequest:HttpClient) { }
  /**
   * @description returns the previous orders
   * @memberof OrderService
   */
  getPreviousOrders=(email:string)=>{
    return this.httpRequest.get(`${environment.baseUrl}/orders?email=${email}`);
  }
}

