import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/cartService/cart.service';
import { Subscription } from 'rxjs';
import { ServiceEmitterService } from 'src/app/serviceEmitter/service-emitter.service';
import { Item } from 'src/app/dashboard/dashboard/dashboard.component';

/**
 *
 * @export
 * @class EcartComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-ecart',
  templateUrl: './ecart.component.html',
  styleUrls: ['./ecart.component.css']
})

export class EcartComponent implements OnInit {
  
  subButton:boolean;
  count:number=1;
  cartItems:Array<any>;
  totalBill:number=0;
  profile:any;
  placeOrderSubscription:Subscription;
  

  constructor(private cartService:CartService,private cartCountEmitter:ServiceEmitterService) { 
    this.getCartItems();
    this.totalBillCalculator();
    this.profile=JSON.parse(sessionStorage.getItem('profile'));
  }

  ngOnInit(): void {
  }

  /**
   *@description get the selected items from  the cart service
   * @memberof EcartComponent
   */
  getCartItems=()=>{
     this.cartItems=this.cartService.selectedItems;
  }
  /**
   * @description Revoves the item from, when user click removes 
   * from cart
   * @memberof EcartComponent
   */
  removeCartItems=(cartItem:Item)=>{
    this.cartItems.splice(this.cartItems.indexOf(cartItem),1);
    this.totalBill=this.totalBill-cartItem.totalBill;
    this.cartCountEmitter.removeCartCount();
  }
  /**
   * @description calculates the price of all items
   * and returns total bill
   * @memberof EcartComponent
   */
  totalBillCalculator=()=>{
    for(let item of this.cartItems){
      this.totalBill=this.totalBill+item.price;
    }
  }
  /**
   * @description: Increases the quantity count, when user clicks on (+) button
   * @memberof EcartComponent
   */
  addQuantity=(item:Item)=>{
    item.quantity=item.quantity+1;
    this.totalBill=this.totalBill+item.price;
    item.totalBill=item.totalBill+item.price;
  }
  /**
   * @description: Decreases the quantity count, when user click on (-) button 
   * @memberof EcartComponent
   */
  substractQuantity=(item:Item)=>{
    console.log(item);
    if(item.quantity>1){
    item.quantity=item.quantity-1;
    this.totalBill=this.totalBill-item.price;
    item.totalBill-=item.price;
   }
  }
  /**
   * @description navigates to payment
   * @memberof EcartComponent
   *
   */
  doPayment=()=>{
    if(sessionStorage.getItem('profile')!==null){
    let bill={
      totalBill:this.totalBill,
      email:this.profile.email,
      date:new Date(),
      cartItems:this.cartItems
    };
    this.placeOrderSubscription=this.cartService.placedOrder(bill).subscribe((response)=>{
      if(response!==null){
        alert('Payment done successfully');
        this.cartItems=[];
        this.totalBill=0;
      }
    },(error)=>{
      alert('Network error! please try after sometime!')
    })
   }
  }
  /**
   * @description unsubscribing placeOrderSubscription
   *
   * @memberof EcartComponent
   */
  ngOnDestroy(){
    if(this.placeOrderSubscription && this.placeOrderSubscription.closed){
        this.placeOrderSubscription.unsubscribe();
    }
  }
}

