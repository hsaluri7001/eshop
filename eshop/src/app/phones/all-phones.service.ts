import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AllPhonesService {
  phonesSubscribe;
  constructor(private httpRequest:HttpClient) { }
  
  /**
   * @description Get the phones from server
   * @memberof AllPhonesService
   */
    getAllPhones=()=>{
    return this.httpRequest.get(`${environment.baseUrl}/phones`);
  }
}
