import { TestBed } from '@angular/core/testing';

import { AllPhonesService } from './all-phones.service';

describe('AllPhonesService', () => {
  let service: AllPhonesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllPhonesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
