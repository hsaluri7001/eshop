import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ServiceEmitterService } from '../serviceEmitter/service-emitter.service';
import { Item } from '../dashboard/dashboard/dashboard.component';
/**
 *
 * @export
 * @class CartService
 */
@Injectable({
  providedIn: 'root'
})

export class CartService {

  selectedItems:Array<any>=[];

  constructor(private httpRequest:HttpClient,
    private cartCountEmitter:ServiceEmitterService) { }
/**
 * @description selected items will processed to the cart
 * 
 * @memberof CartService
 */
addToCart=(item:Item)=>{
    for(let some of this.selectedItems){
    if(item.itemName===some.itemName){
      alert("Item already added to cart");
      return false;
      }
    }
    this.cartCountEmitter.addCartCount();
    this.selectedItems.push(item);
    console.log(this.selectedItems);
  }
  /**
   * @description place the order when user process the payment
   * @memberof CartService
   */
  placedOrder=(order)=>{
   return this.httpRequest.post(`${environment.baseUrl}/orders`,order);
  }
   
}
