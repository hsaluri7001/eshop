import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/userservice/user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ServiceEmitterService } from 'src/app/serviceEmitter/service-emitter.service';
import { Subscription } from 'rxjs';

/**
 *
 *
 * @export
 * @class LoginComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  submitted:boolean=false;
  response:any;
  error:string;
  authenticationSubscribe:Subscription;

  @Output() registrationFlag=new EventEmitter();
  constructor(private userService:UserService,
    private router:Router,
    private matDialog: MatDialog,
    private serviceEmitter:ServiceEmitterService) { }

  ngOnInit(
  ): void {
    this.loginForm=new FormGroup({
      userName:new FormControl('',Validators.required),
      passWord:new FormControl('',Validators.required)
    });
  }
   /**
    * login flag to change view
    */
  loginFlag=()=>{
   this.registrationFlag.emit(false);
  }
  /**
   * @description login is is used to validate the user credentials
   *  if validations passed navigates to dashboard
   * @memberof LoginComponent
   */
  login = () => {
    this.submitted = true;
    let formValue = this.loginForm.value;
    let userName = formValue.userName;
    let passWord = formValue.passWord;
    
    if (this.loginForm.valid) {
    this.authenticationSubscribe= this.userService.getUserAuthentication(userName, passWord).subscribe((response) => {
        this.response=response;
        if(this.response.length===1){
          sessionStorage.setItem('profile',JSON.stringify(this.response[0]));
          this.serviceEmitter.setProfile();
          this.router.navigate(['']);
          this.matDialog.closeAll();
        }
        else{
          this.error="Invalid Credentials!"
        }
      },
        (error) => {
          console.log(error);
          alert('Under maintainence ! please try after some time');
          return false;
        });
    }
  }
  /**
   *@description unsubscribing authenticationSubscribe
   *
   * @memberof LoginComponent
   */
  ngOnDestroy(){
    if(this.authenticationSubscribe && !this.authenticationSubscribe.closed){
        this.authenticationSubscribe.unsubscribe();
    }
  }
}


