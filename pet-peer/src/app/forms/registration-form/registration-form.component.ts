import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UserService } from 'src/app/userService/user.service';
import { applySourceSpanToExpressionIfNeeded } from '@angular/compiler/src/output/output_ast';

/**
 *
 * @export
 * @class RegistrationFormComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  registrationForm: FormGroup;
  submitted: boolean = false;
  signUp: boolean = false;
  login: boolean = true;
  loginPage:boolean=true;
  dashBoard:boolean=false;

  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.minLength(5)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1)]),
      emailId: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[hcl|dbs]+\.[a-z]{2,}$')]),
      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      passWord: new FormControl('', [Validators.required,passWordValidator]),
      gender: new FormControl('', Validators.required)
    });
  }
  register = () => {
    this.submitted = true;
    console.log(this.registrationForm);
    let formValue=this.registrationForm.value;
    if (this.registrationForm.valid) {
      let user={
        firstName:formValue.firstName,
        lastName:formValue.lastName,
        emailId:formValue.emailId,
        phoneNumber:formValue.phoneNumber,
        passWord:formValue.passWord,
        gender:formValue.gender
      }
      this.userService.registerUser(user).subscribe((response)=>{
        alert("User successfully registered. please login")
        if(response!==null){
          this.login=true;
          this.signUp=false;
          this.registrationForm.reset();
        }
      })
    }
  }
  displaySignUp = () => {
    this.login = false;
    this.signUp = true;
  }
  displayLogin = () => {
    this.signUp = false;
    this.login = true;
  }
  updateDashBoard=(flag)=>{
    this.loginPage=false;
    this.dashBoard=true;
 }
}
function passWordValidator(control:AbstractControl):{[key:string]:boolean}|null{
  let passWord=control.value;
  if(passWord != '') {
      if(passWord.length < 6) {
        return {"passWordPattern":true};
      }
      if(passWord == name) {
        return {"passWordPattern":true};
      }
       let re = /[0-9]/;
      if(!re.test(passWord)) {
        return {"passWordPattern":true};
      }
      re = /[a-z]/;
      if(!re.test(passWord)) {
        return {"passWordPattern":true};;
      }
      re = /[A-Z]/;
      if(!re.test(passWord)) {
        return {"passWordPattern":true};
      }
    }
    
    /*  else {
      alert("Error: Please check that you've entered and confirmed your password!");
      return {"passWordPattern":true};
    } */
}
