import { Component,  OnInit, Input,Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, EmailValidator, AbstractControl } from '@angular/forms';
import { UserService } from 'src/app/userService/user.service';
/**
 *
 * @export
 * @class LoginFormComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {
  
  loginForm: FormGroup;
  response:any;
  error:string;
  submitted: boolean = false;
  @Output() loginPage=new EventEmitter;

  constructor(private userService: UserService) {

  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      emailId: new FormControl('', Validators.required),
      passWord: new FormControl('', Validators.required)
    });
  }
  /**
   * @descripton:login form with authentications
   *
   * @memberof LoginFormComponent
   */
  login = () => {
    this.submitted = true;
    let formValue = this.loginForm.value;
    let emailId = formValue.emailId;
    let passWord = formValue.passWord;
    if (this.loginForm.valid) {
      this.userService.getUserAuthentication(emailId, passWord).subscribe((response) => {
        this.response=response;
        if(this.response.length===1){
          this.loginPage.emit(false);
        }
        else{
          this.error="Invalid Credentials!"
        }
      },
        (error) => {
          console.log(error);
          alert("Under maintainence ! please try after some time");
          return false;
        });
    }
  }

}
