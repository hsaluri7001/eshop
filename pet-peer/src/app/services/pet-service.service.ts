import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PetServiceService {

  constructor(private httpRequest: HttpClient) { }

  getAllPets=()=>{
    return this.httpRequest.get(`${environment.baseUrl}/petDetails/`);
  }
  addPetDetails=(petDetails)=>{
    return this.httpRequest.post(`${environment.baseUrl}/petDetails/`,petDetails);
  }
  deletePetDetails=(id:number)=>{
    return this.httpRequest.delete(`${environment.baseUrl}/petDetails/${id}`)
  }
  updatePetDetails=(id:number,petDetails)=>{
    return this.httpRequest.put(`${environment.baseUrl}/petDetails/${id}`,petDetails)
  }
}