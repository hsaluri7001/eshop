import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpRequest:HttpClient) { }

  getUserAuthentication=(emailId,passWord)=>{
    let url:string=`${environment.baseUrl}/users?emailId=${emailId}&&passWord=${passWord}`;
    return this.httpRequest.get(url);
  }
  getUser=(emailId)=>{
    return this.httpRequest.get(`${environment.baseUrl}/users?emailId=${emailId}`);
  }
  registerUser=(user)=>{
    return this.httpRequest.post(`${environment.baseUrl}/users`,user)
  }
}
