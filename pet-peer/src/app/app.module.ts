import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationFormComponent } from './forms/registration-form/registration-form.component';
import { LoginFormComponent } from './forms/login-form/login-form.component';
import { PetDetailsComponent } from './petDetails/pet-details/pet-details.component';
import { PetListComponent } from './petList/pet-list/pet-list.component';
import { HttpClientModule } from '@angular/common/http';
import { PetServiceService } from './services/pet-service.service';
import { ReactiveFormsModule } from '@angular/forms';
import { UserService} from './userService/user.service';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationFormComponent,
    LoginFormComponent,
    PetDetailsComponent,
    PetListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule 
  ],
  providers: [PetServiceService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
