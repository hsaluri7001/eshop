import { Component, OnInit, Input } from '@angular/core';
import { PetServiceService } from 'src/app/services/pet-service.service';

/**
 *
 * @export
 * @class PetListComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {
  listOfPets: any;
  mainPage: boolean = true;
  pet: any;
  updateDetailsFlag: boolean; 
  addPet:boolean;
  constructor(private petService: PetServiceService) {
    this.getPetDetails();
  }
  /**
   *
   * @memberof PetListComponent
   */
  ngOnInit(): void {
  }
  /**
   *
   * @memberof PetListComponent
   */
  getPetDetails = () => {
    this.petService.getAllPets().subscribe((response) => {
      this.listOfPets = response;
    }, (error) => {
      console.log("No RESPONSE");
    });
  }
  
  /**
   * @memberof PetListComponent
   */
  addPetDetailsRecord=(petDetails)=>{
    console.log(petDetails);
    this.petService.addPetDetails(petDetails).subscribe(
      (response)=>{
        alert("Record inserted successfully!");
        this.mainPage = true;
        this.addPet=false;
        this.getPetDetails();
      },
      (error)=>{
        console.log("NetWork issue! please try after some time")
      }
    );
  }
  /**
   * @memberof PetListComponent
   */
  updatePetDetails = (updatePetDetails) => {
    console.log(updatePetDetails);
    this.petService.updatePetDetails(updatePetDetails.id, updatePetDetails).subscribe(
      (response) => {
        this.pet = response;
        this.updateDetailsFlag = false;
      },
      (error) => {
        console.log(error);
      }
    );
  }
 /**
  *
  * @memberof PetListComponent
  */
 trackById = (index: number, user: any) => {
    return user.id;
  }
  /**
   *
   * @memberof PetListComponent
   */
  selectUser = (data) => {
    this.pet = data;
    this.mainPage = false;

  }
  /**
   *
   * @memberof PetListComponent
   */
  deletePetDetails = (id) => {
    this.petService.deletePetDetails(id).subscribe((response) => {
      this.mainPage = true;
      alert("record has been deleted");
      this.getPetDetails();
    },
      (error) => {
        console.log(error);
      })
  }
  /**
   *
   * @memberof PetListComponent
   */
  home = () => {
    this.mainPage = true;
    this.addPet=false;
    this.updateDetailsFlag = false;
    this.addPet=false;
    this.getPetDetails();
    this.pet=false;
  }
  /**
   *
   * @memberof PetListComponent
   */
  addPetDetails=()=>{
   this.addPet=true;
   this.updateDetailsFlag=false;
   this.mainPage=false;
   this.pet=false;
  }
}
