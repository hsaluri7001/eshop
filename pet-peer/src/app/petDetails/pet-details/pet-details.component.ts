import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/**
 * @export
 * @class PetDetailsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-pet-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.css']
})

export class PetDetailsComponent implements OnInit {
  @Input() updateDetailsFlag : boolean;
  @Input() pet;
  @Input() addPet:boolean;
  @Output() deleteRecord=new EventEmitter();
  @Output() updateRecord=new EventEmitter();
  @Output() addRecord=new EventEmitter();

  formGroup:FormGroup;
  constructor() {
    console.log(this.addPet);
  }
/**
 * @memberof PetDetailsComponent
 */
ngOnInit(): void { 
    this.formGroup=new FormGroup({
      petName:new FormControl('',Validators.required),
      petAge:new FormControl('',Validators.required),
      ownerEmailId:new FormControl('',Validators.required),
      ownerAdress:new FormControl('',Validators.required)
    });
  }
 
  /**
   *
   * @memberof PetDetailsComponent
   */
  displayForm=(data)=>{
    console.log(data);
    this.formGroup.patchValue({
      petName:this.pet.petName,
      petAge:this.pet.petAge,
      ownerEmailId:this.pet.ownerEmailId,
      ownerAdress:this.pet.ownerAdress
    });
    this.updateDetailsFlag=!this.updateDetailsFlag;
  }
  /**
   *
   * @memberof PetDetailsComponent
   */
  deletePetDetails=(id)=>{
    console.log(id);
    this.deleteRecord.emit(id);
    this.pet=null;
    this.updateDetailsFlag=false;

  }
  /**
   *
   * @memberof PetDetailsComponent
   */
  updatePetDetails=()=>{
    const formValue=this.formGroup.value;
      const petDetails={
        "id":this.pet.id,
        "petName":formValue.petName,
        "petAge":formValue.petAge,
        "ownerEmailId":formValue.ownerEmailId,
        "ownerAdress":formValue.ownerAdress
      }
    this.updateRecord.emit(petDetails);
    this.updateDetailsFlag=false;
    this.formGroup.reset();
  
  }
  /**
   *
   * @memberof PetDetailsComponent
   */
  addPetDetails=()=>{
    const formValue=this.formGroup.value;
      const petDetails={
        "petName":formValue.petName,
        "petAge":formValue.petAge,
        "ownerEmailId":formValue.ownerEmailId,
        "ownerAdress":formValue.ownerAdress
      }
      this.addRecord.emit(petDetails);
      this.formGroup.reset();
  }
}
