import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-info',
  templateUrl: './student-info.component.html',
  styleUrls: ['./student-info.component.css']
})
export class StudentInfoComponent implements OnInit {

  isStyle1:boolean=false;  
  name:string='harish';
  name1:string="saluri";
  age:number=20;
  isMarried:boolean=false;
  location={city:'banglore'};
  lang:Array<any>=[{name:'english'},{name:'hindhi'},{name:'telugu'}];
  isDisabled:boolean=true;
  
  constructor() { }

  ngOnInit(): void {
  }

  update=()=>{ 
     this.isStyle1=!this.isStyle1;
   }
   updateScreen=(event)=>{
    this.name=event.target.value;
    console.log(this.name);   
   }
   updateValue=(nameRef)=>{
     console.log(nameRef.value);
   }

}
