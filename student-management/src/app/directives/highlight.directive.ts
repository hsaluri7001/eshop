import { Directive, ElementRef, HostListener, Host, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  
  @Input() styleObj:string
  constructor(private element:ElementRef) {
    //this.element.nativeElement.style.backgroundColor='lightblue';
   }
   @HostListener('mouseenter')
   mouseEnter(){
     this.highlight({backgroundColor:'red'});
   }
   @HostListener('mouseleave')
   mouseLeave(){
     
     this.highlight(this.styleObj);
   }
   highlight=(style:any)=>{
     this.element.nativeElement.style.backgroundColor=style.backgroundColor;
   }
}
