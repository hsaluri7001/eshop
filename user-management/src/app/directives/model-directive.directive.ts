import { Directive, ElementRef, HostListener } from '@angular/core';
/**
 *
 * @export
 * @class ModelDirectiveDirective
 */
@Directive({
  selector: '[appModelDirective]'
})

export class ModelDirectiveDirective {

  constructor(private element:ElementRef) { 
    element.nativeElement.style.backgroundColor="#4CAF50";  
   }
  /**
   * @description Changes the color of button,when cursor enters 
   * @memberof ModelDirectiveDirective
   */
  @HostListener("mouseenter")
  changeColorOnHover=()=>{
    this.element.nativeElement.style.backgroundColor="green";
  }
  /**
   *
   * @description Changes the color of button,when cursor leaves
   * @memberof ModelDirectiveDirective
   */
  @HostListener("mouseleave")
  changeColorOnLeave=()=>{
    this.element.nativeElement.style.backgroundColor="#4CAF50"
  }

}
