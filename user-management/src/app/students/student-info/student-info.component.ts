import { Component, OnInit, Output, EventEmitter } from '@angular/core';
/**
 *
 * @export
 * @class StudentInfoComponent
 */
@Component({
  selector: 'app-student-info',
  templateUrl: './student-info.component.html',
  styleUrls: ['./student-info.component.css']
})

export class StudentInfoComponent {
  @Output() selectedUser=new EventEmitter();
  student: Array<any> = [
    {
      id: 101,
      firstName: 'harish',
      lastName: 'saluri',
      address: 'bellampalli',
      mobile: '9700121124',
      emailId: 'saluri@hcl.com'
    },
    {
      id: 102,
      firstName: 'saluri',
      lastName: 'harish',
      address: 'hyderabad',
      mobile: '9700121125',
      emailId: 'saluri@dbs.com'
    }
  ];
  selectedStudent:any;
  columnNames: Array<String> = ["id", "firstName", "lastName", "address", "mobile", "emailId"];
  headers: Array<string> = ["ID", "FIRST NAME", "LAST NAME", "ADDRESS", "MOBILE", "EMAIL ID", "SELECT"];
  constructor() { }

  /**
   *
   * @memberof StudentInfoComponent
   */
  trackById = (index: number, user: any) => {
    return user.id;
  }
  /**
   * @description:select the student
   * @memberof StudentInfoComponent
   * @param{object} 
   */
  selectedStudents=(student)=>{
    this.selectedStudent=student;
  }

}
