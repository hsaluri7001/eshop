import { Component, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
/**
 *
 * @export
 * @class StudentDetailsComponent
 */
@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls:['./student-details.component.css']
 })

export class StudentDetailsComponent {
  @Input() selectedStudent:any;
  headers: Array<string> = ["ID", "FIRST NAME", "LAST NAME", "ADDRESS", "MOBILE", "EMAIL ID"]; 
  constructor() { 
  }

  /**
   * @description:Clears the user
   * @memberof StudentDetailsComponent
   */
  remove=()=>{
    console.log("remove");
    this.selectedStudent=false;
  }

} 
