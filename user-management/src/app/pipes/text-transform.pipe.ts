import { Pipe, PipeTransform } from '@angular/core';
/**
 *
 *
 * @export
 * @class TextTransformPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'textTransform',
  pure:true
})
export class TextTransformPipe implements PipeTransform {

  /**
   *
   * @param {string} value
   * @returns {string}
   * @memberof TextTransformPipe
   */
  transform(value:string): string {
    value="DNA"+value;
    return value;
  }  
}
