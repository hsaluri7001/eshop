import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentInfoComponent } from './students/student-info/student-info.component';
import { StudentDetailsComponent } from './students/student-details/student-details.component';
import { TextTransformPipe } from './pipes/text-transform.pipe';
import { ModelDirectiveDirective } from './directives/model-directive.directive';
import { ReactiveFormComponent } from './forms/reactive-form/reactive-form.component';
import { TemplateFormComponent } from './forms/template-form/template-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    StudentInfoComponent,
    StudentDetailsComponent,
    TextTransformPipe,
    ModelDirectiveDirective,
    ReactiveFormComponent,
    TemplateFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
