import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  registrationForm: FormGroup;
  submitted:boolean = false;

  constructor() { }
  ngOnInit(): void {
  this.registrationForm=new FormGroup({
    firstName: new FormControl('',[Validators.required,Validators.minLength(5)]),
    lastName:new FormControl('',[Validators.required,Validators.minLength(1)]),
    emailId:new FormControl('',[Validators.required,Validators.pattern('[a-z0-9._%+-]+@[hcl|dbs]+\.[a-z]{2,}$')]),
    phoneNumber:new FormControl('',[Validators.required]),
    role:new FormControl('',Validators.required),
    gender:new FormControl('',Validators.required)
  });
  }
  register=()=>{
    this.submitted=true;
    console.log(this.registrationForm);
    if(this.registrationForm.valid){
      console.log('next step');
    }
  }


}
